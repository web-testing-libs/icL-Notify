#include "ownnotify.h"

#include <QDebug>
#include <QGuiApplication>
#include <QThread>
#include <QTimer>

int main(int argc, char * argv[]) {
    QGuiApplication a(argc, argv);

    OwnNotify man;

    man.pushMessage("User", "ev", "User", "Message");
    man.pushGroupMessage("User", "ev", "Group", "User", "Message");
    man.pushCommunityMessage(
      "User", "ev", "Community", "Group", "User", "Message");
    man.pushGroupAdded("User", "ev", "Group", "User");
    man.pushCommunityAdded("User", "ev", "Community", "Group", "User");

    man.pushActions(
      "sd", "s", "Actions", "Test actions",
      {{"ok", "Ok"}, {"cancel", "Cancel"}});

    man.pushImage1(
      "", "", "image", {}, "file:///C:/Users/Win10/Downloads/icon.png", "alt");

    QThread::msleep(3000);

    //    man.clear();

    QGuiApplication::connect(&man, &Notify::clicked, &a, QGuiApplication::quit);

    QTimer::singleShot(100000, &a, QGuiApplication::quit);

    return QGuiApplication::exec();
}
