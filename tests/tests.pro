TARGET = icL-notify-tests
TEMPLATE = app

QT = core gui widgets

DESTDIR = $$PWD/../bin

CONFIG += c++17 no_include_pwd console

!win32 {
    QMAKE_CXXFLAGS += -std=c++17
}
else {
    QMAKE_CXXFLAGS += /std:c++17
    QMAKE_LFLAGS += /MACHINE:X64
}

DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000

!win32 {
    LIBS += -L$$PWD/../bin/lib
    LIBS += -l-icl-notify
}
else {
    LIBS += "$$PWD/../bin/-icl-notify.lib"
}

SOURCES += \
    main.cpp \
    ownnotify.cpp

HEADERS += \
    ownnotify.h
