#include "notify.h"

#include <QDebug>
#include <QImage>
#include <QStringBuilder>
#include <QTimer>
#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusMessage>
#include <QtDBus/QDBusMetaType>

Notify::Notify(const QString & appName, QObject * parent)
    : QObject(parent)
    , dbus(
        "org.freedesktop.Notifications", "/org/freedesktop/Notifications",
        "org.freedesktop.Notifications", QDBusConnection::sessionBus(), this)
    , appName(appName) {
    qDBusRegisterMetaType<QImage>();

    QDBusConnection::sessionBus().connect(
      "org.freedesktop.Notifications", "/org/freedesktop/Notifications",
      "org.freedesktop.Notifications", "ActionInvoked", this,
      SLOT(actionInvoked(uint, QString)));
    QDBusConnection::sessionBus().connect(
      "org.freedesktop.Notifications", "/org/freedesktop/Notifications",
      "org.freedesktop.Notifications", "NotificationClosed", this,
      SLOT(notificationClosed(uint, uint)));

    QDBusMessage capabilitiesMess =
      dbus.call(QDBus::AutoDetect, "GetCapabilities");
    QStringList capabilites = capabilitiesMess.arguments()[0].toStringList();

    m_supportBodyImages  = capabilites.contains("body-images");
    m_supportBodyMark    = capabilites.contains("body-markup");
    m_supportHyperlinks  = capabilites.contains("body-hyperlinks");
    m_supportPersistence = capabilites.contains("persistence");
    m_supportSound       = capabilites.contains("sound");
}

Notify::~Notify() = default;

uint Notify::pushNotification(const Notification & notif) {
    uint id;

    if (notif.subheader.isNull()) {
        id = showNotification(notif.header, notif.text, notif);
    }
    else if (m_supportBodyMark) {
        id = showNotification(
          notif.header, "<b>" % notif.subheader % "</b>\n" % notif.text, notif);
    }
    else {
        id =
          showNotification(notif.header, notif.subheader % notif.text, notif);
    }

    notifications[id] = notif;

    return id;
}

uint Notify::pushAttach(const Notification & notif) {
    uint id;

    QString imgTag = R"(<img src=")" % notif.attach.path() % R"(" alt=")" %
                     notif.subheader % R"(">)";

    if (notif.text.isNull()) {
        id = showNotification(notif.header, imgTag, notif);
    }
    else if (m_supportBodyImages) {
        id = showNotification(notif.header, notif.text % '\n' % imgTag, notif);
    }
    else {
        id = showNotification(
          notif.header,
          notif.text % '\n' %
            tr("Your system doesn't support images in push notifications"),
          notif);
    }

    notifications[id] = notif;

    return id;
}

uint Notify::showNotification(
  const QString summary, const QString text, const Notification & notif) {
    QVariantMap     hints;
    QList<QVariant> argumentList;

    if (!notif.contentIconImage.isNull())
        hints["image-data"] = notif.contentIconImage;

    if (!notif.contentIconPath.isNull())
        hints["image-path"] = notif.contentIconPath;

    if (m_supportPersistence)
        hints["transient"] = notif.resident;

    if (m_supportSound)
        hints["suppress-sound"] = notif.silent;

    argumentList << appName;              // app_name
    argumentList << uint(0);              // replace_id
    argumentList << notif.unixIcon;       // app_icon
    argumentList << summary;              // summary
    argumentList << text;                 // body
    argumentList << parseActions(notif);  // actions
    argumentList << hints;                // hints
    argumentList << timeout;              // timeout in ms

    QDBusMessage reply =
      dbus.callWithArgumentList(QDBus::AutoDetect, "Notify", argumentList);
    if (reply.type() == QDBusMessage::ErrorMessage) {
        qDebug() << "D-Bus Error:" << reply.errorMessage();
        return 0;
    }
    else {
        return reply.arguments().first().toUInt();
    }
}

QStringList Notify::parseActions(const Notification & notif) {
    QStringList ret;

    ret << "default"
        << "Default";

    for (auto it = notif.actions.begin(); it != notif.actions.end(); it++) {
        ret << (*it).first << (*it).second;
    }

    return ret;
}

void Notify::hideNotification(uint id) {
    QList<QVariant> argumentList = {id};

    auto find = notifications.find(id);

    if (find != notifications.end()) {
        dbus.callWithArgumentList(
          QDBus::AutoDetect, "CloseNotification", argumentList);

        emit closed(find.value());

        notifications.remove(id);
    }
}

void Notify::clear() {
    QList<uint> notificationsIds;

    for (auto it = notifications.begin(); it != notifications.end(); it++) {
        notificationsIds << it.key();
    }

    for (uint notification : notificationsIds) {
        hideNotification(notification);
    }
}

void Notify::actionInvoked(uint id, const QString & action) {
    if (!notifications.contains(id))
        return;

    if (action == "default") {
        emit clicked(notifications[id]);
    }
    else {
        emit triggered(notifications[id], action);
    }

    notifications.remove(id);
}

void Notify::notificationClosed(uint id, uint reason) {
    if (!notifications.contains(id))
        return;

    if (reason != 3) {
        notifications.remove(id);
    }
    else {
        // The notification will be shown later, destroy it after a minute
        QTimer::singleShot(60000, [this, id]() { this->hideNotification(id); });
    }
}

QDBusArgument & operator<<(QDBusArgument & arg, const QImage & image) {
    if (image.isNull()) {
        arg.beginStructure();
        arg << 0 << 0 << 0 << false << 0 << 0 << QByteArray();
        arg.endStructure();
        return arg;
    }

    QImage scaled = image.scaledToHeight(100, Qt::SmoothTransformation);
    scaled        = scaled.convertToFormat(QImage::Format_ARGB32);

#if Q_BYTE_ORDER == Q_LITTLE_ENDIAN
    // ABGR -> ARGB
    QImage i = scaled.rgbSwapped();
#else
    // ABGR -> GBAR
    QImage i(scaled.size(), scaled.format());
    for (int y = 0; y < i.height(); ++y) {
        QRgb * p   = (QRgb *)scaled.scanLine(y);
        QRgb * q   = (QRgb *)i.scanLine(y);
        QRgb * end = p + scaled.width();
        while (p < end) {
            *q = qRgba(qGreen(*p), qBlue(*p), qAlpha(*p), qRed(*p));
            p++;
            q++;
        }
    }
#endif

    arg.beginStructure();
    arg << i.width();
    arg << i.height();
    arg << i.bytesPerLine();
    arg << i.hasAlphaChannel();
    int channels = i.isGrayscale() ? 1 : (i.hasAlphaChannel() ? 4 : 3);
    arg << i.depth() / channels;
    arg << channels;
    arg << QByteArray(
      reinterpret_cast<const char *>(i.bits()),
      static_cast<int>(i.sizeInBytes()));
    arg.endStructure();
    return arg;
}

const QDBusArgument & operator>>(const QDBusArgument & arg, QImage &) {
    // This is needed to link but shouldn't be called.
    Q_ASSERT(0);
    return arg;
}
